

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function firstFunction(){
		    let fullName = prompt("What is your name?");
			let age = prompt("How old are you?");

			let loc = prompt("Where do you live?");

			console.log("Hello, " + fullName);
			console.log("You are " + age + "years old.");
			console.log("You live in " + loc)

			alert("Thank you for your input!");

	}

	firstFunction();
		
	

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function displayArtists(){
		console.log("1. Taylor Swift");
		console.log("2. TWICE");
		console.log("3. Carly Rae Jepsen");
		console.log("4. FLETCHER");
		console.log("5. Lady Gaga");
	}

	displayArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function displayMovies(){
		console.log("1. My life as a Zucchini");
		console.log("Rotten Tomatoes Rating: 99%");

		console.log("2. The Tale");
		console.log("Rotten Tomatoes Rating: 99%");

		console.log("3. The Wailing ");
		console.log("Rotten Tomatoes Rating: 99%");

		console.log("4. Sunset Blvd. ");
		console.log("Rotten Tomatoes Rating: 98%");

		console.log("5. Titanic");
		console.log("Rotten Tomatoes Rating: 88%");

	}
	
	displayMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function printUsers(){
	alert("Hi! Please add the names of your friends.");

	let friend1 = prompt("Enter your first friend's name:"); 

	let friend2 = prompt("Enter your second friend's name:");

	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printUsers();

